<?php

/**
 * Class CodeStats
 *
 * @author phandle <philipp@flips-soft.at>
 * @website https://www.flips-soft.at
 *
 */
class CodeStats {

    public $levelFactor = 0.025;
    public $url         = 'https://codestats.net/api/users/%s';
    public $user        = '';
    public $data        = [];
    public $lines       = [];
    public $separator   = '-----------------------';

    public function __construct( $user ) {
        // Create Url with Username
        $this->url = sprintf( $this->url, $user );

        // Fetch Data from API
        $this->fetchData();

        // Loop trough all Languages
        foreach( $this->data->languages as $language => $languageData ) {
            $currentLevel = intval( $this->levelFactor * sqrt( $languageData->xps ) );
            $xpNextLevel = ( pow( ( $currentLevel + 1 ) / $this->levelFactor, 2 ) );

            // If we are not Level 0, calculate stuff for Levelup
            if( (int) $currentLevel > 0 ) {
                $xpCurrentLevel = ( pow( ( $currentLevel ) / $this->levelFactor, 2 ) );
                $percentageNextLevel = ( ( $languageData->xps - $xpCurrentLevel ) / ( $xpNextLevel - $xpCurrentLevel ) ) * 100;
            } else {
                $percentageNextLevel = ( $languageData->xps / $xpNextLevel ) * 100;
            }

            // We want to Sort it properly, highest Level -> highest Progress at same Level
            $index = $languageData->xps;
            $showLanguages[$index] = [
                'language'            => $language,
                'currentLevel'        => $currentLevel,
                'percentageNextLevel' => floor( $percentageNextLevel )
            ];
        }

        // Key sort
        krsort( $showLanguages );
        $this->data->languages = $showLanguages;
    }

    public function fetchData() {
        // Get and encode Data
        $this->data = json_decode( file_get_contents( $this->url ) );

        // Add Current Level
        $this->data->currentLevel = intval( $this->levelFactor * sqrt( $this->data->total_xp ) );

        // Add XP to current + next Level
        $this->data->xpCurrentLevel = ( pow( ( $this->data->currentLevel ) / $this->levelFactor, 2 ) );
        $this->data->xpNextLevel = ( pow( ( $this->data->currentLevel + 1 ) / $this->levelFactor, 2 ) );

        // Add Percentage next Level
        $this->data->percentageNextLevel = ( ( $this->data->total_xp - $this->data->xpCurrentLevel ) / ( $this->data->xpNextLevel - $this->data->xpCurrentLevel ) ) * 100;

        // Calc Average xp Gain
        $days = 0;
        $gained = 0;
        foreach( $this->data->dates as $date => $gainedDay ) {
            $gained += $gainedDay;
            $days++;
        }
        $this->data->averagePerDay = $gained / $days;

        // Add XP yesterday
        $this->data->xpYesterday = intval( $this->data->dates->{@date( 'Y-m-d', time() - 86400 )} );
    }

}

// Tracking
$user = $_GET['user'] ?? 'phandle';
if( isset( $_SERVER['HTTP_REFERER'] ) === true ) {
    file_get_contents( 'https://piwik.flips-soft.at/piwik.php?idsite=5&action_name=banner-' . $user . '&rec=1&e_c=banner-gen&e_a=referer&e_n=' . $_SERVER['HTTP_REFERER'] );
} else {
    file_get_contents( 'https://piwik.flips-soft.at/piwik.php?idsite=5&action_name=banner-' . $user . '&rec=1&e_c=banner-gen&e_a=referer&e_n=direct' );
}
$r = file_get_contents( 'https://piwik.flips-soft.at/piwik.php?idsite=5&rec=1&action_name=banner-' . $user . '&e_c=banner-gen&e_a=ip&e_n=' . $_SERVER['REMOTE_ADDR'] );

// include composer autoload
require 'vendor/autoload.php';

// Banner
$width = 560;
$height = 200;
$logoWidthHeight = 50;

// Load Data
$data = new CodeStats( $user );

$imagine = new Imagine\Imagick\Imagine();

$palette = new Imagine\Image\Palette\RGB();
$size = new Imagine\Image\Box( $width, $height );
$color = $palette->color( '3e4053', 100 );
$image = $imagine->create( $size, $color );

// Add Background
$image->draw()->polygon( [
        new Imagine\Image\Point( 0, 0 ),
        new Imagine\Image\Point( $width, 0 ),
        new Imagine\Image\Point( $width, $logoWidthHeight ),
        new Imagine\Image\Point( 0, $logoWidthHeight )
    ], $palette->color( 'ffffff' ), true, 0 );

// Add Logo
$logo = $imagine->open( './resources/logo_codestats.png' )
    ->resize( new \Imagine\Image\Box( $logoWidthHeight, $logoWidthHeight ) );
$image->paste( $logo, new Imagine\Image\Point( 10, 0 ) );

// Place user
//create Text Object with font-family, font-size and color from settings
$TextFont = new \Imagine\Gd\Font( './resources/Lato2OFL/Lato-Regular.ttf', 25, $palette->color( '000' ) );
//create a Box (dimensions) based on font and given string...
$text = sprintf( '%s - Level %s', $user, $data->data->currentLevel );
$textBox = $TextFont->box( $text );

$image->draw()->text( $text, $TextFont, new Imagine\Image\Point( $width - $textBox->getWidth() - 10, 5 ) );

// Place Languages
$TextFontLanguages = new \Imagine\Gd\Font( './resources/Lato2OFL/Lato-Regular.ttf', 15, $palette->color( 'fff' ) );

$yOffset = $logoWidthHeight + 5;
$i = 1;
foreach( $data->data->languages as $index => $langData ) {
    $languageText = sprintf( '%s - Level %s', $langData['language'], $langData['currentLevel'] );
    $languageTextBox = $TextFontLanguages->box( $languageText );

    $image->draw()->text( $languageText, $TextFontLanguages, new Imagine\Image\Point( 10, $yOffset ) );

    $yOffset += $languageTextBox->getHeight() + 4;
    if( $i === 6 ) {
        break;
    }
    $i++;
}

// Get Data per Day
$dataDays = [];

$timestamp = time() - 6 * 24 * 60 * 60;
$userDates = (array) $data->data->dates;
while( count( $dataDays ) < 7 ) {
    if( isset( $userDates[date( 'Y-m-d', $timestamp )] ) === false ) {
        $dataDays[date( 'D', $timestamp )] = 0;
    } else {
        $dataDays[date( 'D', $timestamp )] = $userDates[date( 'Y-m-d', $timestamp )];
    }
    $timestamp += 24 * 60 * 60;
}

// Place Chart
$chartWidth = 300;
$chartHeight = $height - $logoWidthHeight - 5;
$url = sprintf( 'https://image-charts.com/chart?chco=ffffff90&chf=bg,s,3e4053&cht=lc&chd=t:%s&chxt=x,y&chxl=0:|%s|1:|100|50|100&chds=a&chs=%sx%s&chg=0,0,0,0&chm=B,ffffff50,0,0,0', implode( ',', $dataDays ), implode( '|', array_keys( $dataDays ) ), $chartWidth, $chartHeight );
$chartImage = $imagine->open( $url );

$image->paste( $chartImage, new Imagine\Image\Point( $width - $chartWidth, $height - $chartHeight ) );

$image->show( 'jpg' );

