<?php

return [
    'name'     => 'Black Eye',
    'smallest' => 'Kreuzer',
    'coins'    => [
        'Dukaten' => [ 10, 'Silbertaler', 1000 ],
        'Silbertaler' => [ 10, 'Heller', 100 ],
        'Heller'  => [ 10, 'Kreuzer', 10 ],
        'Kreuzer' => 10
    ]
];