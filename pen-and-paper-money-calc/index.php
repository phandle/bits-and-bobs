<?php

// Search for all Users
$users = glob( './data/*.json' );
// Search for all Currencies
$currencies = glob( './currencies/*.php' );

// Step 1
if( isset( $_GET['user'] ) === true && empty( $_GET['user'] ) === false ) {
    // User File for Data
    $userFile = sprintf( './data/%s.json', strtolower( $_GET['user'] ) );
    // If he is a new User, create that File
    if( file_exists( $userFile ) === false ) {
        file_put_contents( $userFile, json_encode( [ 'name' => $_GET['user'] ] ) );
    }
    // Load User Data
    $userData = json_decode( file_get_contents( $userFile ) );
}
// Step 2
if( isset( $_GET['currency'] ) === true && empty( $_GET['currency'] ) === false ) {
    // User File for Data
    $userFile = sprintf( './data/%s.json', strtolower( $_GET['user'] ) );
    // Include Currency Data
    $currency = include( sprintf( './currencies/%s.php', strtolower( $_GET['currency'] ) ) );
    // If User hasn't this Currency, add it
    if( isset( $userData->{$currency['name']} ) === false ) {
        $userData->{$currency['name']} = 0;
        file_put_contents( $userFile, json_encode( $userData ) );
    }
}
// Do the Banking!
if( isset( $_POST['banking'] ) === true ) {
    foreach( $_POST['coins'] as $coin => $amount ) {
        if( empty( $amount ) === true ) {
            continue;
        }
        // Get the conversion Rate
        $conversion = ( is_array( $currency['coins'][$coin] ) === true ) ? $currency['coins'][$coin][2] : 1;
        if( isset( $_POST['add'] ) === true ) {
            $userData->{$currency['name']} += $amount * $conversion;
        } else {
            $userData->{$currency['name']} -= $amount * $conversion;
        }
    }
    // Save it to UserData
    file_put_contents( $userFile, json_encode( $userData ) );
    // Redirect
    header( sprintf( 'Location: ?user=%s&currency=%s', $_GET['user'], $_GET['currency'] ) );
}

// Helper
function getCleanName( string $name ) : string {
    return urlencode( strtolower( str_replace( ' ', '_', $name ) ) );
}

?>
<html>
<head>
    <title>Money Calc</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container mt-4">
    <h1>Money Calc</h1>
    <?php if( isset( $_GET['user'] ) === false || empty( $_GET['user'] ) === true ) : ?>
        <h2>Select User</h2>
        <ul>
            <?php foreach( $users as $user ) : ?>
                <?php $user = json_decode( file_get_contents( $user ) ); ?>
                <li><a href="?user=<?php echo $user->name; ?>"><?php echo $user->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <form method="GET">
            <div class="form-group">
                <input type="text" name="user" placeholder="Username" class="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary">Next</button>
        </form>
    <?php elseif( isset( $_GET['currency'] ) === false || empty( $_GET['currency'] ) === true ) : ?>
        <h2><?php echo $userData->name; ?></h2>
        <h3>Select Currency</h3>
        <ul>
            <?php foreach( $currencies as $currency ) : ?>
                <?php $currency = include( $currency ); ?>
                <li>
                    <a href="?user=<?php echo $_GET['user']; ?>&currency=<?php echo getCleanName( $currency['name'] ); ?>"><?php echo $currency['name']; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else : ?>
        <h2><?php echo $userData->name; ?> (<?php echo $currency['name']; ?>)</h2>
        <div class="alert alert-info">
            <small>
                Conversion:
                <?php foreach( $currency['coins'] as $coin => $conversion ) : ?>
                    <?php if( is_array( $conversion ) === true ) : ?>
                        <br/><?php echo sprintf( '1 x %s = %s x %s', $coin, $conversion[0], $conversion[1] ); ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </small>
        </div>
        <div class="alert alert-<?php echo ( $userData->{$currency['name']} < 0 ) ? 'danger' : 'success'; ?>">
            <?php
            $convertedCoins = [];
            $negative = ( $userData->{$currency['name']} < 0 ) ? true : false;
            $money = ( $negative === true ) ? abs( $userData->{$currency['name']} ) : $userData->{$currency['name']};
            foreach( $currency['coins'] as $coin => $conversion ) {
                if( is_array( $conversion ) === true ) {
                    $convertedCoins[] = sprintf( '%s %s', floor( $money / $conversion[2] ), $coin );
                    $money -= floor( $money / $conversion[2] ) * $conversion[2];
                } else {
                    $convertedCoins[] = sprintf( '%s %s', $money, $coin );
                }
            }
            ?>
            <?php echo sprintf( ( $negative === true ) ? 'Your debts %s!' : 'You have %s!', implode( ', ', $convertedCoins ) ); ?>
        </div>
        <form method="post">
            <input type="hidden" name="banking" value="1"/>
            <?php foreach( $currency['coins'] as $coin => $conversion ) : ?>
                <div class="form-group">
                    <input type="number" pattern="\d*" name="coins[<?php echo $coin; ?>]"
                           placeholder="<?php echo $coin; ?>" value="" class="form-control"/>
                </div>
            <?php endforeach; ?>
            <input type="submit" name="add" class="btn btn-success" value="Add"/>
            <input type="submit" name="remove" class="btn btn-danger" value="Remove"/>
            <a href="/pen-and-paper-money-calc" class="btn btn-secondary">Back</a>
        </form>
    <?php endif; ?>
</div>
</body>
</html>