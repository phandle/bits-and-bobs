# Bits and Bobs

## Code::Stats - PHP Printer
For console printing like [Geektool](https://www.tynsoe.org/v2/geektool/)

![Geektool Output2](https://gitlab.com/phandle/bits-and-bobs/raw/master/php-printer/screenshots/final-output.png)

## Code::Stats - Banner generator
WIP Banner generator for Codestats

![Demo phandle](https://gitlab.com/phandle/bits-and-bobs/raw/master/codestats-banner/screenshots/banner_phandle.jpeg)

## Little Calculator for Currency Banking in Pen & Paper