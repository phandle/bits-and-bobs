# Little PHP script to display Stats from Code::Stats
## Scriptsettings
Change this Line to your Username

![Geektool Settings](https://gitlab.com/phandle/bits-and-bobs/raw/master/php-printer/screenshots/settings.png)
## Settings to Display PHP Script
![Geektool Settings](https://gitlab.com/phandle/bits-and-bobs/raw/master/php-printer/screenshots/geektool-settings.png)
## Output 1  
![Geektool Output1](https://gitlab.com/phandle/bits-and-bobs/raw/master/php-printer/screenshots/geektool-output.png)
## Output 2
![Geektool Output2](https://gitlab.com/phandle/bits-and-bobs/raw/master/php-printer/screenshots/final-output.png)
