<?php

/**
 * Class CodeStats
 *
 * @author phandle <philipp@flips-soft.at>
 * @website https://www.flips-soft.at
 *
 */
class CodeStats {

    public $levelFactor = 0.025;
    public $url         = 'https://codestats.net/api/users/%s';
    public $user        = '';
    public $data        = [];
    public $lines       = [];
    public $separator   = '-----------------------';

    public function __construct( $user ) {
        // Create Url with Username
        $this->url = sprintf( $this->url, $user );

        // Fetch Data from API
        $this->fetchData();

        // Create First Line with current Level, XP left to next Levle
        $this->addLine( sprintf( 'Level %s (%sXP / %s%%)', $this->data->currentLevel, number_format( $this->data->xpNextLevel - $this->data->total_xp, 0, '', '.' ), floor( $this->data->percentageNextLevel ) ) );
        // Add Line with XP Yesterday and Today
        $this->addLine( sprintf( 'T: %sXP / A: %sXP', number_format( $this->data->new_xp, 0, '', '.' ), number_format( $this->data->averagePerDay, 0, '', '.' ) ) );
        // Add Separatorline
        $this->addLine( $this->separator );

        // Loop trough all Languages
        foreach( $this->data->languages as $language => $languageData ) {
            $currentLevel = intval( $this->levelFactor * sqrt( $languageData->xps ) );
            $xpNextLevel = ( pow( ( $currentLevel + 1 ) / $this->levelFactor, 2 ) );

            // If we are not Level 0, calculate stuff for Levelup
            if( (int) $currentLevel > 0 ) {
                $xpCurrentLevel = ( pow( ( $currentLevel ) / $this->levelFactor, 2 ) );
                $percentageNextLevel = ( ( $languageData->xps - $xpCurrentLevel ) / ( $xpNextLevel - $xpCurrentLevel ) ) * 100;
            } else {
                $percentageNextLevel = ( $languageData->xps / $xpNextLevel ) * 100;
            }

            // Left Part of Line with Languagetitle
            $partLeft = sprintf( '%s', $language );
            // Right PArt of Line with Level and Progress
            $partRight = sprintf( 'Lvl %s | %s%%', $currentLevel, str_pad( floor( $percentageNextLevel ), 2, '0', STR_PAD_LEFT ) );

            // We want to Sort it properly, highest Level -> highest Progress at same Level
            $index = ( ( $currentLevel + 1 ) * 1000 ) + floor( $percentageNextLevel );
            // Create Line with Spaces between
            $showLanguages[$index . '_' . $language] = sprintf( '%s%s', str_pad( $partLeft, strlen( $this->separator ) - strlen( $partRight ), ' ' ), $partRight );
        }

        // Key sort
        krsort( $showLanguages );
        // Add multiple Lines
        $this->addLines( $showLanguages );

        // Finally print it
        $this->printLines();
    }

    public function fetchData() {
        // Get and encode Data
        $this->data = json_decode( file_get_contents( $this->url ) );

        // Add Current Level
        $this->data->currentLevel = intval( $this->levelFactor * sqrt( $this->data->total_xp ) );

        // Add XP to current + next Level
        $this->data->xpCurrentLevel = ( pow( ( $this->data->currentLevel ) / $this->levelFactor, 2 ) );
        $this->data->xpNextLevel = ( pow( ( $this->data->currentLevel + 1 ) / $this->levelFactor, 2 ) );

        // Add Percentage next Level
        $this->data->percentageNextLevel = ( ( $this->data->total_xp - $this->data->xpCurrentLevel ) / ( $this->data->xpNextLevel - $this->data->xpCurrentLevel ) ) * 100;

        // Calc Average xp Gain
        $days = 0;
        $gained = 0;
        foreach( $this->data->dates as $date => $gainedDay ) {
            $gained += $gainedDay;
            $days++;
        }
        $this->data->averagePerDay = $gained / $days;

        // Add XP yesterday
        $this->data->xpYesterday = intval( $this->data->dates->{@date( 'Y-m-d', time() - 86400 )} );
    }

    public function addLine( $line ) {
        $this->lines[] = $line;
    }

    public function addLines( $strings ) {
        foreach( $strings as $string ) {
            $this->addLine( $string );
        }
    }

    public function printLines() {
        echo implode( "\n", $this->lines );
    }

}

// Init and Let it go ;-)
( new CodeStats( 'phandle' ) );
